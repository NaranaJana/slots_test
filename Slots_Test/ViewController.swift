//
//  ViewController.swift
//  Slots_Test
//
//  Created by Julius on 02.07.18.
//  Copyright © 2018 com.test.test. All rights reserved.
//

import UIKit

enum Combinations: Int{
    case leafs = 75
    case sucks = 50
    case spades = 35
    case hearts = 25
    case clubs = 15
    case diamonds = 10
}

enum Components: String{
    case leafs = "combination_1"
    case sucks = "combination_2"
    case spades = "combination_3"
    case hearts = "combination_4"
    case clubs = "combination_5"
    case diamonds = "combination_6"
    case shoe = "combination_7"
}

class ViewController: UIViewController {
    @IBOutlet weak var slotsPickerView: UIPickerView!
    @IBOutlet weak var spinButton: UIButton!
    @IBOutlet weak var jackpotLabel: UILabel!
    @IBOutlet weak var coinsLabel: UILabel!
    @IBOutlet weak var betLabel: UILabel!
    
    var slotComponents = [String]()
    var timer = Timer()
    
//    Variables for infinite scroll
    private let totalRows = 100_000
    var component = 0
    
//    Variables for coins
    var currentCoins = 995{
        didSet{
            coinsLabel.text = "\(currentCoins)"
        }
    }
    var bet = 5{
        didSet{
            betLabel.text = "\(bet)"
        }
    }
    
//    Jackpot win values
    let single = 5
    let double = 10
    var jackpot = 100000{
        didSet{
            jackpotLabel.text = "\(jackpot)"
        }
    }
    
//    Variables to detect components
    var first = ""
    var second = ""
    var third = ""
    
//    Show stats view
    let wonView: WonView = .fromNib()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    
    func setupUI(){
        self.view.backgroundColor = UIColor(patternImage: UIImage(named: "background")!)
        let components = ["combination_1", "combination_2", "combination_3", "combination_4", "combination_5", "combination_6", "combination_7"]
        for _ in 0..<totalRows{
            let component = components[Int(arc4random_uniform(7))]
            slotComponents.append(component)
        }
        
        for i in 0..<3{
            slotsPickerView.selectRow(totalRows / 2, inComponent: i, animated: false)
        }
        
        srandom(UInt32(time(nil)))
    }

    @IBAction func lessBetAction(_ sender: UIButton) {
        if bet > 5{
            bet -= 5
            currentCoins += 5
        }
    }
    
    @IBAction func moreBetAction(_ sender: UIButton) {
        if currentCoins >= 0 && bet < 100{
            bet += 5
            currentCoins -= 5
        }
    }

    @IBAction func spinAction(_ sender: UIButton) {
        first = ""
        second = ""
        third = ""
        currentCoins -= bet
        
        timer = Timer.scheduledTimer(timeInterval: 0.3, target: self, selector: #selector(setRandom), userInfo: nil, repeats: true)
    }
    
    @objc func setRandom(){
        let row = Int(arc4random_uniform(UInt32(totalRows)))
        slotsPickerView.selectRow(row, inComponent: component, animated: true)
        pickerView(slotsPickerView, didSelectRow: row, inComponent: component)
        component += 1
        if component == 3{
            timer.invalidate()
            component = 0
        }
    }
    
    func checkForCombination(first: String, second: String, third: String){
        if third == second && second == first && first == Components.clubs.rawValue{
            showView(withCoins: Combinations.clubs.rawValue)
            currentCoins += Combinations.clubs.rawValue
        }else if third == second && second == first && first == Components.diamonds.rawValue{
            showView(withCoins: Combinations.diamonds.rawValue)
            currentCoins += Combinations.diamonds.rawValue
        }else if third == second && second == first && first == Components.hearts.rawValue{
            showView(withCoins: Combinations.hearts.rawValue)
            currentCoins += Combinations.hearts.rawValue
        }else if third == second && second == first && first == Components.spades.rawValue{
            showView(withCoins: Combinations.spades.rawValue)
            currentCoins += Combinations.spades.rawValue
        }else if third == second && second == first && first == Components.leafs.rawValue{
            showView(withCoins: Combinations.leafs.rawValue)
            currentCoins += Combinations.leafs.rawValue
        }else if third == second && second == first && first == Components.sucks.rawValue{
            showView(withCoins: Combinations.sucks.rawValue)
            currentCoins += Combinations.sucks.rawValue
        }else if third == second && second == first && first == Components.shoe.rawValue{
            showView(withCoins: jackpot)
            currentCoins += jackpot
            jackpot = 0
        }else if first == Components.shoe.rawValue || second == Components.shoe.rawValue || third == Components.shoe.rawValue{
            showView(withCoins: bet * single)
            currentCoins += bet * single
        }else if second == first && first == Components.shoe.rawValue || third == first && first == Components.shoe.rawValue || third == second && second == Components.shoe.rawValue{
            showView(withCoins: bet * double)
            currentCoins += bet * double
        }else if third != second && second != first{
            jackpot += bet
        }
    }
    
    func showView(withCoins coins: Int){
        
        wonView.coinsLabel.text = "\(coins)"
        wonView.frame = CGRect(x: self.view.bounds.width / 2 - 75, y: self.view.bounds.height / 2 - 50, width: 150, height: 150)
        UIView.animate(withDuration: 1.5) {
            self.view.addSubview(self.wonView)
        }
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        let touch = touches.first
        let point = touch!.location(in: self.view)

        if let viewWithTag = self.view.viewWithTag(12) {
            viewWithTag.removeFromSuperview()
        }
    }
}

extension ViewController: UIPickerViewDelegate, UIPickerViewDataSource{
    func pickerView(_ pickerView: UIPickerView, rowHeightForComponent component: Int) -> CGFloat {
        let image = UIImage(named: slotComponents[0])
        return image!.size.height
    }
    
    func pickerView(_ pickerView: UIPickerView, widthForComponent component: Int) -> CGFloat {
        let image = UIImage(named: slotComponents[0])
        return image!.size.width
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        switch component {
        case 0:
            first = slotComponents[row]
        case 1:
            second = slotComponents[row]
        case 2:
            third = slotComponents[row]
        default:
            break
        }
        
        checkForCombination(first: first, second: second, third: third)
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return totalRows
    }
    
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        return UIImageView(image: UIImage(named: slotComponents[row]))
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 3
    }
}

// NU I NAHUYA YA ETO DELAL BLAT?first
