//
//  WonView.swift
//  Slots_Test
//
//  Created by Maksym on 03.07.2018.
//  Copyright © 2018 com.test.test. All rights reserved.
//

import UIKit

class WonView: UIView{
    @IBOutlet weak var coinsLabel: UILabel!
    @IBOutlet weak var greenView: UIView!
    
    class func fromNib<T: UIView>() -> T {
        return Bundle.main.loadNibNamed(String(describing: T.self), owner: nil, options: nil)![0] as! T
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.greenView.layer.cornerRadius = 3
    }
}
